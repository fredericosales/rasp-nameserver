#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br
# 2016
#

NAMESERVER="tanenbaum"

# updating
clear;
echo "Updating...";
echo;
sudo apt -f install;
sudo apt update;
sudo apt -y upgrade;

# install dependencies
clear;
echo "Install dependencies...";
echo;
sudo apt -y install bind9 p7zip-full dnsutils dhcp3-server;
